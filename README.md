Macaroons POC
------

# What is this
This is a POC demonstrating the interaction between IAM, Auth and API by passing macaroons around. There are 2 services running: iam and auth. You can run them respectively in different terminals like the following: `node iam.js` and `node auth.js`. Running api.js will send requests to iam and auth to get the root macaroon and third party macaroons, respectively. It allows the verifier (in this case, IAM again) to have no knowledge of third party caveats but still able to verify it.

This POC depends on the macaroons.js Javascript library. More info about macaroons can be found here: http://macaroons.io/

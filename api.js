'use strict';

const async = require('async');
const crypto = require('crypto');
const macaroons = require('macaroons.js');
const request = require('request');

const SECRET_LENGTH = macaroons.MacaroonsConstants.MACAROON_SUGGESTED_SECRET_LENGTH;

const MacBuilder = macaroons.MacaroonsBuilder;

function register(username, password, callback) {
  request({
    method: 'POST',
    uri: 'http://localhost:8001/register',
    json: true,
    body: {
      username,
      password,
    },
  }, (err, response, body) => {
    if (err) {
      return callback(err);
    }
    return callback(null, { userId: body });
  });
}

function mintMacaroon(userId, callback) {
  request({
    method: 'POST',
    uri: 'http://localhost:8000/mint',
    json: true,
    body: {
      userId,
    },
  }, (err, response, body) => {
    if (err) {
      return callback(err);
    }
    return callback(null, body);
  });
}

function authUser(username, password, caveatKey, callback) {
  request({
    method: 'POST',
    uri: 'http://localhost:8001/authenticate',
    json: true,
    body: {
      username,
      password,
      caveatKey,
    },
  }, (err, response, body) => {
    if (err) {
      return callback(err);
    }
    return callback(null, body);
  });
}

function verifyMacaroon(userId, rootMac, dischargeMac, callback) {
  request({
    method: 'POST',
    uri: 'http://localhost:8000/verify',
    json: true,
    body: {
      userId,
      rootMac,
      dischargeMac,
    },
  }, (err, response, body) => {
    if (err) {
      return callback(err);
    }
    return callback(null, body);
  });
}

const username = 'alice';
const password = 'password';

// register a user with auth service
register(username, password, (err1) => {
  if (err1) {
    return console.error(err1);
  }

  // generate a random caveat key that only auth and we know about
  const caveatKey = crypto.randomBytes(SECRET_LENGTH / 2).toString('hex');
  return authUser(username, password, caveatKey, (err2, res2) => {
    if (err2) {
      return console.error(err2);
    }
    const userId = res2.userId;
    const id = res2.id;   // 3rd party macaroon identifier
    const dischargeMac = res2.dischargeMac;  // discharge macaroon to prove user is authenticated

    // generate a macaroon with first party caveat for this userId
    return mintMacaroon(userId, (err3, res3) => {
      if (err3) {
        return console.error(err3);
      }

      let mac = MacBuilder.deserialize(res3);
      // add 3rd party caveat to root mac
      mac = MacBuilder.modify(mac)
              .add_third_party_caveat('localhost/auth', caveatKey, id)
              .getMacaroon();
      const d = MacBuilder.deserialize(dischargeMac);
      const dp = MacBuilder.modify(mac)
                   .prepare_for_request(d)
                   .getMacaroon();

      // check the macaroon caveats are verified
      // 1. first party: userId
      // 2. third party: timestamp
      // 3. third party: user auth
      return verifyMacaroon(userId, mac.serialize(), dp.serialize(), (err4, res4) => {
        if (err4) {
          return console.error(err4);
        }
        return console.log(res4);
      });
    });
  });
});

'use strict';

exports.validationError = function(request, reply, source, error) {
  console.error(error);
  return reply(error);
};

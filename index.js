'use strict';

const crypto = require('crypto');
const macaroons = require('macaroons.js');
const uuid = require('uuid');

const MacBuilder = macaroons.MacaroonsBuilder;
const MacVerifier = macaroons.MacaroonsVerifier;
const TimestampCaveatVerifier = macaroons.verifier.TimestampCaveatVerifier;
const MACAROON_SUGGESTED_SECRET_LENGTH = macaroons.MacaroonsConstants.MACAROON_SUGGESTED_SECRET_LENGTH;

const location = 'http://www.example.org';
const secretKey = crypto.randomBytes(MACAROON_SUGGESTED_SECRET_LENGTH/2).toString('hex');
const identifier = uuid.v4();

// Serialize and deserialize

let macaroon = new MacBuilder(location, secretKey, identifier)
               .getMacaroon();

let serialized = macaroon.serialize();

macaroon = MacBuilder.deserialize(serialized);

let verifier = new MacVerifier(macaroon);

let valid = verifier.isValid(secretKey);

console.log(valid);

// Adding first-party caveats

macaroon = new MacBuilder(location, secretKey, identifier)
           .add_first_party_caveat('account = 123')
           .getMacaroon();

serialized = macaroon.serialize();

macaroon = MacBuilder.deserialize(serialized);

verifier = new MacVerifier(macaroon);
verifier.satisfyExact('account = 123');

valid = verifier.isValid(secretKey);

console.log(valid);

// Adding third party caveats

const location2 = 'http://mybank/';
const secret2 = 'this is a different super-secret key; never use the same secret twice';
const publicIdentifier = uuid.v4();
const mb = new MacBuilder(location2, secret2, publicIdentifier)
    .add_first_party_caveat('account = 3735928559');

const caveatKey = '4; guaranteed random by a fair toss of the dice';
// const predicate = 'user = Alice';
// send_to_3rd_party_location_and_do_auth(caveat_key, predicate);
// identifier = recv_from_auth();
const identifier2 = 'this was how we remind auth of key/pred';
const m = mb.add_third_party_caveat('http://auth.mybank/', caveatKey, identifier2)
    .getMacaroon();

const d = new MacBuilder('http://auth.mybank/', caveatKey, identifier2)
    .add_first_party_caveat('time < 2018-01-01T00:00')
    .getMacaroon();

const dp = MacBuilder.modify(m)
    .prepare_for_request(d)
    .getMacaroon();

verifier = new MacVerifier(m);

verifier
  .satisfyExact('account = 3735928559')
  .satisfyGeneral(TimestampCaveatVerifier)
  .satisfy3rdParty(dp);

valid = verifier.isValid(secret2);

console.log(valid);

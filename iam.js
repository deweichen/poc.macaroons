'use strict';

const crypto = require('crypto');
const macaroons = require('macaroons.js');
const Hapi = require('hapi');
const Joi = require('joi');
const uuid = require('uuid');
const clone = require('clone');
const config = require('config');
const utils = require('./utils');

const MacBuilder = macaroons.MacaroonsBuilder;
const MacVerifier = macaroons.MacaroonsVerifier;
const TimestampCaveatVerifier = macaroons.verifier.TimestampCaveatVerifier;

const SECRET_LENGTH = macaroons.MacaroonsConstants.MACAROON_SUGGESTED_SECRET_LENGTH;
const LOCATION = 'localhost/iam';
const SECRET_KEY = crypto.randomBytes(SECRET_LENGTH / 2);  // use buffer instead of string for better performance

const server = new Hapi.Server();

server.connection(clone(config.get('iam-server')));

server.route({
  method: ['POST'],
  path: '/mint',
  config: {
    auth: false,
    description: 'Mint a fresh macaroon',
    validate: {
      payload: {
        userId: Joi.string().required(),
      },
      failAction: utils.validationError,
    },
    handler: (request, reply) => {
      const id = uuid.v4();
      const mac = new MacBuilder(LOCATION, SECRET_KEY, id)
                    .add_first_party_caveat(`userId = ${request.payload.userId}`)
                    .getMacaroon();
      return reply(mac.serialize());
    },
  },
});

server.route({
  method: ['POST'],
  path: '/verify',
  config: {
    auth: false,
    description: 'Verify if macaroon is valid',
    validate: {
      payload: {
        userId: Joi.string().required(),
        rootMac: Joi.string().required(),
        dischargeMac: Joi.string().optional(),
      },
      failAction: utils.validationError,
    },
    handler: (request, reply) => {
      const m = MacBuilder.deserialize(request.payload.rootMac);
      const verifier = new MacVerifier(m);

      verifier
        .satisfyExact(`userId = ${request.payload.userId}`)
        .satisfyGeneral(TimestampCaveatVerifier);

      if (request.payload.dischargeMac) {
        const d = MacBuilder.deserialize(request.payload.dischargeMac);
        verifier.satisfy3rdParty(d);
      }
      return reply(verifier.isValid(SECRET_KEY));
    },
  },
});

server.start((err) => {
  if (err) {
    return console.log(err);
  }
  return console.log('Server started at:', server.info.uri);
});

'use strict';

const macaroons = require('macaroons.js');
const Hapi = require('hapi');
const Joi = require('joi');
const uuid = require('uuid');
const clone = require('clone');
const config = require('config');

const MacBuilder = macaroons.MacaroonsBuilder;

const LOCATION = 'localhost/auth';

const server = new Hapi.Server();

server.connection(clone(config.get('auth-server')));

const cache = server.cache({
  segment: 'users',
  expiresIn: 3 * 24 * 60 * 60 * 1000,
});
server.app.cache = cache;

server.route({
  method: ['POST'],
  path: '/register',
  config: {
    auth: false,
    description: 'Create a new user',
    validate: {
      payload: {
        username: Joi.string().required(),
        password: Joi.string().required(),
      },
    },
    handler: (request, reply) => {
      const obj = {
        username: request.payload.username,
        password: request.payload.password,
        userId: uuid.v4(),
      };
      request.server.app.cache.set(request.payload.username, obj, 0, (err) => {
        if (err) {
          return reply(err);
        }
        return reply(obj.userId);
      });
    },
  },
});

server.route({
  method: ['POST'],
  path: '/authenticate',
  config: {
    auth: false,
    description: 'Authenticate a user',
    validate: {
      payload: {
        username: Joi.string().required(),
        password: Joi.string().required(),
        caveatKey: Joi.string().required(),
      },
    },
    handler: (request, reply) => {
      request.server.app.cache.get(request.payload.username, (err, cached) => {
        if (err) {
          return reply(err);
        }
        if (cached.password !== request.payload.password) {
          return reply(new Error('Wrong password'));
        }
        const id = uuid.v4();
        const d = new MacBuilder(LOCATION, request.payload.caveatKey, id)
                    .add_first_party_caveat('time < 2052-01-01T00:00')
                    .getMacaroon();
        return reply({
          id,
          userId: cached.userId,
          dischargeMac: d.serialize(),
        });
      });
    },
  },
});

server.start((err) => {
  if (err) {
    return console.error(err);
  }
  return console.log('Server started at:', server.info.uri);
});
